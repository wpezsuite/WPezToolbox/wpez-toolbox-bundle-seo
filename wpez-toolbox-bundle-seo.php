<?php
/*
Plugin Name: WPezToolbox - Bundle: SEO
Plugin URI: https://gitlab.com/wpezsuite/wpeztoolbox/wpez-toolbox-bundle-seo
Description: SEO - A WPezToolbox bundle of WordPress SEO plugins
Version: 0.0.2
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: wpez_tbox
*/

namespace WPezToolboxBundleSEO;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

add_filter( 'WPezToolboxLoader\plugins', __NAMESPACE__. '\plugins');


function plugins( $arr_in = [] ) {

	$str_bundle           = __( 'SEO', 'wpez_tbox' );
	$str_prefix_separator = ' | ';
	$str_prefix           = $str_bundle . $str_prefix_separator;

	$arr = [

		'all-in-one-seo-pack' =>
			[
				'name'     => $str_prefix . 'All in One SEO Pack',
				'slug'     => 'all-in-one-seo-pack',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Michael Torbert',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/hallsofmontezuma/',
						'desc'    => 'Use All in One SEO Pack to optimize your WordPress site for SEO. It’s easy and works out of the box for beginners, and has advanced features and an API for developers.',
						'url_img' => 'https://ps.w.org/all-in-one-seo-pack/assets/icon-128x128.png?rev=2075006',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/all-in-one-seo-pack/',
						'url_repo' => 'https://github.com/semperfiwebdesign/all-in-one-seo-pack',
						'url_site' => 'https://semperplugins.com/',
						'url_premium' => 'https://semperplugins.com/all-in-one-seo-pack-pro-version/',
						'url_fb'   => 'https://www.facebook.com/semperfiwebdesign',
						'url_tw'   => 'https://twitter.com/aioseopack'
					]
				]
			],

		'all-in-one-schemaorg-rich-snippets' =>
			[
				'name'     => $str_prefix . 'Schema – All In One Schema Rich Snippets',
				'slug'     => 'all-in-one-schemaorg-rich-snippets',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Brainstorm Force',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/brainstormforce/',
						'desc'    => 'Get eye catching results in search engines with the most popular schema markup plugin. Easy implementation of schema types like Review, Events, Recipes, Article, Products, Services etc',
						'url_img' => 'https://ps.w.org/all-in-one-schemaorg-rich-snippets/assets/icon-128x128.png?rev=1569211',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/all-in-one-schemaorg-rich-snippets/',
						'url_repo' => 'https://github.com/brainstormforce/all-in-one-schemaorg-rich-snippets',
						'url_site' => 'https://wpschema.com/',
						'url_premium' => 'https://wpschema.com/pricing/',
						'url_fb'   => 'https://www.facebook.com/BrainstormForce/',
						'url_tw'   => 'https://twitter.com/WeBrainstorm'
					]
				]
			],

		'autodescription' =>
			[
				'name'     => $str_prefix . 'The SEO Framework',
				'slug'     => 'autodescription',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'The SEO Framework Team (Sybre Waaijer)',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/cybr/',
						'desc'    => 'The only feature-complete SEO plugin that follows the white-hat guidelines and rules imposed by WordPress and search engines. Start using proven methods to optimize your website for SEO. Simple, dedicated, extensible, unrestricted, ad-free, and no strings attached.',
						'url_img' => 'https://ps.w.org/autodescription/assets/icon.svg?rev=2153256',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/autodescription/',
						'url_repo' => 'https://github.com/sybrew/the-seo-framework',
						'url_site' => 'https://cyberwire.nl/',
						'url_tw'   => 'https://twitter.com/SybreWaaijer'
					]
				]
			],

		'redirection' =>
			[
				'name'     => $str_prefix . 'Redirection',
				'slug'     => 'redirection',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'John Godley',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/johnny5/',
						'desc'    => 'With Redirection you can easily manage 301 redirections, keep track of 404 errors, and generally tidy up any loose ends your site may have. This can help reduce errors and improve your site ranking.',
						'url_img' => 'https://secure.gravatar.com/avatar/ef843b3f2ab21efa3ffb0eb1ced4dd18?s=100&d=mm&r=g',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/redirection/',
						'url_repo' => 'https://github.com/johngodley/redirection',
						'url_site' => 'https://johngodley.com/',
						'url_tw'   => 'https://twitter.com/all_noodles'
					]
				]
			],

		'seo-by-rank-math' =>
			[
				'name'     => $str_prefix . 'WordPress SEO Plugin',
				'slug'     => 'seo-by-rank-math',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Rank Math',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/rankmath/',
						'desc'    => 'SEO is the most consistent source of traffic for any website. We created Rank Math, a WordPress SEO plugin, to help every website owner get access to the SEO tools they need to improve their SEO and attract more traffic to their website.',
						'url_img' => 'https://ps.w.org/seo-by-rank-math/assets/icon.svg?rev=2034417',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/seo-by-rank-math/',
						'url_site' => 'https://rankmath.com/',
						'url_fb'   => 'https://www.facebook.com/groups/rankmathseopluginwordpress/'
					]
				]
			],

		'wp-seopress' =>
			[
				'name'     => $str_prefix . 'SEOPress',
				'slug'     => 'wp-seopress',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'SEOPress (Benjamin Denis)',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/rainbowgeek/',
						'desc'    => 'SEOPress is a powerful plugin to optimize your SEO, boost your traffic, improve social sharing, build custom HTML and XML Sitemaps, create optimized breadcrumbs, add schemas / Google Structured data types, manage redirections 301 and so much more.',
						'url_img' => 'https://ps.w.org/wp-seopress/assets/icon-128x128.png?rev=1993062',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/wp-seopress/',
						'url_repo' => 'https://github.com/wp-seopress/wp-seopress-public',
						'url_site' => 'https://www.seopress.org/',
						'url_premium' => 'https://www.seopress.org/pricing/',
						'url_fb'   => 'https://www.facebook.com/groups/seopress/',
						'url_tw'   => 'https://twitter.com/wp_seopress'
					]
				]
			],


		'squirrly-seo' =>
			[
				'name'     => $str_prefix . 'Squirrly SEO 2019',
				'slug'     => 'squirrly-seo',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Squirrly SEO',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/cifi/',
						'desc'    => 'Squirrly looks at each page on your WordPress site the same way Google does. It translates everything for you with simple red and green elements. You’ll get the answers you were waiting for. This is the only plugin and SEO software capable of adapting to the specifics of each page and the context it has inside a website. It provides answers for you, after very complex analysis made on multiple levels.',
						'url_img' => 'https://ps.w.org/squirrly-seo/assets/icon-128x128.png?rev=972207',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/squirrly-seo/',
						'url_site' => 'https://www.squirrly.co/wordpress/',
						'url_premium' => 'https://plugin.squirrly.co/squirrly-seo-pricing/',
						'url_fb'   => 'https://www.facebook.com/Squirrly.co',
						'url_tw'   => 'https://twitter.com/SquirrlyHQ'
					]
				]
			],

		'wordlift' =>
			[
				'name'     => $str_prefix . 'WordLift – AI powered SEO',
				'slug'     => 'wordlift',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'WordLift, Insideout10',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/wordlift/',
						'desc'    => 'WordLift helps you organize posts and pages adding facts, links and media to build beautifully structured websites, for both humans and search engines. WordLift lets you create, own and publish your own knowledge graph. WordLift publishes your content as Linked Open Data following Tim Berners-Lee‘s Linked Data Principles.',
						'url_img' => 'https://ps.w.org/wordlift/assets/icon.svg?rev=2116919',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/wordlift/',
						'url_repo' => 'https://github.com/insideout10/wordlift-plugin',
						'url_site' => 'https://wordlift.io/',
						'url_premium' => 'https://wordlift.io/pricing/',
						'url_fb'   => 'https://www.facebook.com/wordlift/',
						'url_tw'   => 'https://twitter.com/wordliftit'
					]
				]
			],

		'wordpress-seo' =>
			[
				'name'     => $str_prefix . 'Yoast SEO',
				'slug'     => 'wordpress-seo',
				'required' => false,
				'wpez'     => [

					'info' => [
						'by'      => 'Team Yoast',
						'by_alt' => 'WP.org Profile',
						'url_by'  => 'https://profiles.wordpress.org/yoast/',
						'desc'    => 'This WordPress SEO plugin helps you with your search engine optimization. Yoast’s mission is SEO for Everyone, the plugin’s users range from the bakery around the corner to some of the most popular sites on the planet.',
						'url_img' => 'https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641',
					],

					'resources' => [
						'url_wp_org' => 'https://wordpress.org/plugins/wordpress-seo/',
						'url_repo' => 'https://github.com/Yoast/wordpress-seo',
						'url_site' => 'https://yoast.com/',
						'url_premium' => 'https://yoast.com/reasons-to-upgrade',
						'url_tw'   => 'https://twitter.com/yoast'
					]
				]
			],

	];

	$arr_mod = apply_filters( __NAMESPACE__ . '\plugins', $arr );

	if ( ! is_array( $arr_mod ) ) {
		$arr_mod = $arr;
	}

	$arr_new = array_values( $arr_mod );

	if ( is_array( $arr_in ) ) {

		return array_merge( $arr_in, $arr_new );
	}

	return $arr_new;
}