## WPezToolbox - Bundle: SEO

__SEO - A WPezToolbox bundle of WordPress SEO plugins.__

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Important

The plugins contained in WPezToolbox bundles are simply "you might consider these suggestions." They are not formal endorsements; there is no claim being made about their quality and/or functionality. Also, some links might be affiliate links. 


### Overview

See the README here: https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader


### This Bundle Includes

- https://wordpress.org/plugins/all-in-one-seo-pack/

- https://wordpress.org/plugins/all-in-one-schemaorg-rich-snippets/

- https://wordpress.org/plugins/autodescription/   (The SEO Framework)

- https://wordpress.org/plugins/redirection/

- https://wordpress.org/plugins/seo-by-rank-math/

- https://wordpress.org/plugins/wp-seopress/

- https://wordpress.org/plugins/squirrly-seo/

- https://wordpress.org/plugins/wordlift/

- https://wordpress.org/plugins/wordpress-seo/ (aka Yoast SEO)


### Helpful Links

- https://gitlab.com/wpezsuite/WPezToolbox

- https://gitlab.com/wpezsuite/WPezToolbox/wpez-toolbox-loader

- https://blog.hubspot.com/website/best-wordpress-seo-plugins

- https://www.wpbeginner.com/showcase/9-best-wordpress-seo-plugins-and-tools-that-you-should-use/

- https://kinsta.com/blog/best-seo-plugins-for-wordpress/

- https://themegrill.com/blog/best-wordpress-seo-plugins/


### TODO 



### CHANGE LOG

- v0.0.2 - 20 January 2020
   - UPDATED: Typo in Rank Math array key

- v0.0.1 - 4 January 2020
   - UPDATED: README's This Bundle Includes

- v0.0.0 - 22 November 2019
   - Proof of Concept

